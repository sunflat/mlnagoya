package msxtool

import org.scalatest.FunSuite
import MsxTextUtil._
import msxtool.example.SamplePresentation1
import java.io._
import scala.io.Source

class PresentationTestSuite extends FunSuite {
	test("test Presentation library") {
		val code=SamplePresentation1.generateCode(SamplePresentation1.pages)
		val lineNumberedCode=convertToLineNumberedCode(code)
		val msxCode=convertToMsxText(codeToString(lineNumberedCode),true)
		
		// update test data:
		//   sbt "run -o src/test/resources/msxtool/SamplePresentation1.bas" => select msxtool.example.SamplePresentation1
		
		// compare
		assert(code ===
			Source.fromInputStream(getClass().getResourceAsStream("SamplePresentation1.bas.phase1.txt")).getLines.toArray)
		assert(lineNumberedCode ===
			Source.fromInputStream(getClass().getResourceAsStream("SamplePresentation1.bas.phase2.txt")).getLines.toArray)
		assert(msxCode ===
			readInputStreamAndClose(getClass().getResourceAsStream("SamplePresentation1.bas")))
	}
}