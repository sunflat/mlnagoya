package msxtool

import org.scalatest.FunSuite
import MsxTextUtil._

class MsxTextUtilSuite extends FunSuite {
	test("test convertToMsxText") {
		assert(convertToMsxText("hoge") === Array('h'.toByte, 'o'.toByte, 'g'.toByte, 'e'.toByte))
		assert(convertToMsxText("ho\nge") === Array('h'.toByte, 'o'.toByte, 0x0D.toByte, 0x0A.toByte, 'g'.toByte, 'e'.toByte))
		assert(convertToMsxText("ホケ") === Array(0xCE.toByte, 0xB9.toByte))
		assert(convertToMsxText("ホゲ") === Array(0xCE.toByte, 0xB9.toByte, 0xDE.toByte))
		assert(convertToMsxText("ぽけ") === Array(0xEE.toByte, 0xDF.toByte, 0x99.toByte))
		assert(convertToMsxText("●○◆■・") === Array(0x85.toByte, 0x84.toByte, 0x83.toByte, 0xFF.toByte, 0xA5.toByte))
		assert(convertToMsxText("月×├",true) === Array(0x01.toByte, 0x41.toByte, 0x01.toByte, 0x5C.toByte, 0x01.toByte, 0x54.toByte, 0x1A.toByte))
	}
	
	test("test convertToLineNumberedProgram") {
		val result=convertToLineNumberedCode(splitToLines("""
'' Test Program
@LOOP
PRINT "HELLO @World"
' LOOP
GOTO @LOOP
""")).toArray
		
		assert(result===splitToLines(
"""10 ' Test Program
20 PRINT "HELLO @World"
30 GOTO 20
"""))
	}
	
	test("test stringLiteral") {
		assert(stringLiteral("hoge\"hoge\nhoge")===""""hoge"+CHR$(34)+"hoge"+CHR$(13)+"hoge"""")
	}
}