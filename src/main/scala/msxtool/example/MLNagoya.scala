package msxtool.example

import msxtool.Presentation
import msxtool.MsxTextUtil._

/**
 * ML Nagoya のスライド
 */
object MLNagoya extends Presentation {
	
	val pages = Seq(
	
	screen3Page(
		setPos(8,24),
		text("MSX"),
		text("Language"),
		inlineCode("COLOR 1,15:LINE(0,120)-(256,192),15,BF"),
		setPos(36,140), text("Sunflat"),
		setPos(40,140), text("Sunflat")
	),
		
	screen2Page("じこしょうかい",
		text("● Twitter ID: Sunflat"),
		vspace1,
		text("● iPhoneや Androidむけの"),
		text("  ゲームを かいはつ"),
		vspace1,
		text("● だいがくの しごとも すこし"),
		vspace2,
		text("● すきな プログラミング げんご:"),
		pause,
		vspace1,
		inlineCode("COLOR 9"),
		text("  MSX-BASIC"),
		moveByCharSize(-1.0/8, -1.0),
		text("  MSX-BASIC")
	),
	
	screen2Page("MSXとは?",
		text("● 1983年に、マイクロソフトと アスキーによって"),
		text("  ていしょうされた、パソコンの とういつきかく"),
		text("  ● CPU: Z80A 8ビット 3.58MHz"),
		text("  ● RAM: 8K ~ 64KB (MSX1)"),
		pause,
		vspace2,
		text("● ようするに、20-30年まえの PC"),
		vspace2,
		text("● さまざまな メーカが MSXパソコンを"),
		text("  はつばい していた"),
		text("  ● むかし、 CASIOのやつを つかっていた")
	),
	
	screen2Page("MSX Language",
		text("● MSXには、 MSX-BASIC という"),
		text("  プログラミング かんきょうが"),
		text("  ひょうじゅんとうさい"),
		vspace1,
		text("● ひさしぶりに、 MSX-BASIC で"),
		text("  プログラムを かいてみよう!!"),
		pause,
		vspace2,
		text("=> MSX-BASICで、 このプレゼンの"),
		text("   スライドを つくってみた")
	),
	
	screen2Page("スライドひょうじ の プログラムれい",
		setCharSize(8,12),
		multiLineText("""10 DEFINT A-Z
20 OPEN"GRP:" FOR OUTPUT AS #1
25 'ページ No.1
30 SCREEN 2:COLOR 15,4,7:CLS
40 LINE(3,3)-(251,19),,B
50 PRESET(8,8):PRINT #1,"MSXとは
   ?"
60 PRESET(8,34):PRINT #1,"● 19
   83年に、マイクロソフトと アスキーによって"
70 PRESET(8,48):PRINT #1,"  てい
   しょうされた、PCの とういつきかく"
80 A$=INPUT$(1)
"""
		)
	),
	
	screen2Page("しんこくな もんだいてん - その1",
		text("● プログラムを にゅうりょく しづらい"),
		vspace1,
		text("  ● じゆうに スクロールできない"),
		vspace1,
		text("  ● クリップボードなんて あるわけない"),
		vspace1,
		text("  ● JISかな はいれつでの、 にほんご"),
		text("    にゅうりょくを しいられる")
	),
	
	screen2Page("しんこくな もんだいてん - その2",
		text("● じつは MSX-BASICは つかいにくい"),
		vspace1,
		text("  ● かんすうを ていぎ できない"),
		vspace1,
		text("  ● すべての へんすうが グローバル へんすう"),
		vspace1,
		text("  ● へんすうの なまえは 2もじまで")
	),
	
	screen3Page(
		setPos(8,80),
		text("""やってられない!""")
	),
		
	screen2Page("Scalaで クロスかいはつ しよう",
		text("● Scala: JVMで うごく たきのう げんご"),
		vspace1,
		text("● Scalaで かんたんな DSLをつくり、"),
		text("  スライドの データを ていぎ"),
		vspace1,
		text("● これをもとに、 MSX-BASICの コードを"),
		text("  Scalaで じどうせいせい")
	),
	
	screen3Page(text("デモ")),
	
	screen2Page("まとめ",
		text("● MSX-BASICで プレゼンしてみた"),
		vspace1,
		text("● これからは、 MSXの ソフトも"),
		text("  Scalaで かいはつする じだい"),
		vspace1,
		text("● さくせいした ツールは ここに:"),
		text("  https://bitbucket.org"),
		text("    /sunflat/mlnagoya"),
		vspace2,
		text("● 「てのかかる げんごほど かわいい」")
	),
	
	screen2Page("END")
	)
	
	def main(args:Array[String]) {
		run(args, pages)
	}
}
