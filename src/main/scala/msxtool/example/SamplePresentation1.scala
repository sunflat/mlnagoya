package msxtool.example

import msxtool.Presentation
import msxtool.MsxTextUtil._

/**
 * テスト用のサンプルスライド
 */
object SamplePresentation1 extends Presentation {
	
	val pages = Seq(
	
	screen2Page("タイトル",
		text("● ほげ"),
		text("  ● ほげほげ"),
		pause,
		vspace1,
		inlineCode("COLOR 9"),
		text("● ほげほげほげ")
	),

	screen3Page(
		text("END")
	)
	
	)
	
	def main(args:Array[String]) {
		run(args, pages)
	}
}
