package msxtool

import MsxTextUtil._
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Buffer
import scala.collection.mutable.HashMap
import scala.math.round
import java.io._

/**
 * スライドをMSX BASICに変換するためのライブラリです
 */
trait Presentation {
	
	case class PageContext(charSize:(Int,Int), cursorPos:(Int,Int), code:Seq[String] = Seq()) {
		def |>(f: PageItem): PageContext = f(this) 
		def |>(fs: Iterable[PageItem]): PageContext = fs.foldLeft(this)(_ |> _)
	}
	
	type PageItem = PageContext => PageContext
	
	case class PresentationContext(pageIndex:Int)
	
	type Page = PresentationContext => PageContext
	
	// page templates
	
	def screenNPage(n:Int)(title:String, body:PageItem*):Page = prc => {
		PageContext((8,14), (8,34)) |> 
			inlineCode("PI="+prc.pageIndex+":TL$="+stringLiteral(title)+":GOSUB @SCREEN"+n+"_INIT") |>
			body |>
			pause
	}
	
	def screen2Page = screenNPage(2) _
	def screen5Page = screenNPage(5) _
	def screen8Page = screenNPage(8) _
	
	def screen3Page(body:PageItem*):Page = prc => {
		PageContext((8*4,10*4), (8,8)) |>
			inlineCode("PI="+prc.pageIndex+":GOSUB @SCREEN3_INIT") |>
			body |>
			pause
	}
	
	// page items
	
	def setCharSize(size:(Int,Int)) : PageItem = _.copy(charSize = size)
	
	def setPos(pos:(Int,Int)) : PageItem = _.copy(cursorPos = pos)
	
	def inlineCode(codeToAppend:String*) : PageItem = ctx => ctx.copy(code = ctx.code ++ codeToAppend)
	
	def pause : PageItem = inlineCode("GOSUB @PAUSE")
	
	def drawText(text:String) : PageItem = ctx => ctx |> inlineCode(
			"PRESET(" + ctx.cursorPos._1 + "," + ctx.cursorPos._2 + "):" +
			"PRINT #1," + stringLiteral(text))
	
	def text(s:String) : PageItem = _ |>
		drawText(s) |>
		moveByCharSize(0,1)
	
	def multiLineText(s:String) : PageItem = ctx =>
		splitToLines(s).foldLeft(ctx)(_ |> text(_))
	
	def moveByCharSize(offset:(Double,Double)) : PageItem = ctx => ctx |>
		setPos(
			ctx.cursorPos._1 + round(ctx.charSize._1 * offset._1).toInt, 
			ctx.cursorPos._2 + round(ctx.charSize._2 * offset._2).toInt)
	
	val vspace1 = moveByCharSize(0, 1.0/2)
	
	val vspace2 = moveByCharSize(0, 1.0)
	
	// generator
	
	def generateCode(pages: Iterable[Page]): Array[String] = {
		val code=new ArrayBuffer[String]
		
		// header
		code ++= Seq("GOTO @INIT")
		
		// pages
		for((page,i) <- pages.zipWithIndex) {
			code ++= Seq("'","@PAGE"+i)
			code ++= page(PresentationContext(i)).code
		}
		
		// sub routines
		code ++= ("""END
'
@INIT
DEFINT A-Z:OPEN "GRP:" FOR OUTPUT AS #1
@SELECT_PAGE
SCREEN 1:COLOR 15,4,7:PRINT "Page";PI;"/";"""+pages.size+""";:INPUT PI$:IF PI$<>"" THEN PI=VAL(PI$)
@JUMP_PAGE
ON PI+1 GOTO """ + ((0 until pages.size).map("@PAGE"+_.toString).mkString(","))+"""
END
'
@SCREEN2_INIT
SCREEN 2:COLOR 15,4,7:CLS
@PAGE_TITLE
LINE(3,3)-(251,19),,B:PRESET(8,8):PRINT #1,TL$
RETURN
@SCREEN3_INIT
SCREEN 3:COLOR 15,4,7:CLS:RETURN
@SCREEN5_INIT
SCREEN 5:COLOR 15,4,7:CLS:GOTO @PAGE_TITLE
@SCREEN8_INIT
SCREEN 8:COLOR 255,&H27,&HEB:CLS:GOTO @PAGE_TITLE
'
@PAUSE
K$=INPUT$(1)
IF K$=CHR$(&H1E) THEN RETURN @SELECT_PAGE
IF K$=CHR$(&H1D) THEN PI=PI-1:RETURN @JUMP_PAGE
RETURN
""").split("\n")
		
		//
		code.toArray
	}
		
	def output(out:OutputStream, data:Array[Byte]) {
		if (out!=null) {
			out.write(data)
			if (out!=System.out && out!=System.err) out.close()
		}
	}
	
	/**
	 * 実行してファイル出力
	 */
	def run(args:Array[String], pages:Iterable[Page]) {
		val fname=if (args.size==2 && args(0)=="-o") args(1) else null
		val out=if (fname==null) System.out else new FileOutputStream(fname)
		val out1=if (fname==null) null else new FileOutputStream(fname+".phase1.txt")
		val out2=if (fname==null) null else new FileOutputStream(fname+".phase2.txt")
		//
		val code=generateCode(pages)
		output(out1,codeToString(code).getBytes("UTF-8"))
		//
		val lineNumberedCode=convertToLineNumberedCode(code)
		output(out2,codeToString(lineNumberedCode).getBytes("UTF-8"))
		//
		val msxCode=convertToMsxText(codeToString(lineNumberedCode),true)
		output(out,msxCode)
	}
}
