package msxtool

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Buffer
import scala.collection.mutable
import scala.math._
import scala.io.Source

/**
 * MSX BASIC言語のテキストを扱います
 */
object MsxTextUtil {
	val CharTable1="♠♥♣♦○●をぁぃぅぇぉゃゅょっ#あいうえおかきくけこさしすせそ#。「」、・ヲァィゥェォャュョッ"+
			"ーアイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワン゛゜たちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわん#■"
	val CharTable2="###################ゔ##がぎぐげござじずぜぞ################"+
			"###ヴ##ガギグゲゴザジズゼゾダヂヅデド#####バビブベボ#################だぢづでど#####ばびぶべぼ"
	val CharTable3="################################################"+
			"##########################パピプペポ###########################ぱぴぷぺぽ"
	val CharTable4="#月火水木金土日年円時分秒百千万π┴┬┤├┼│─┌┐└┘×大中小"
	val CharMap = mutable.Map.empty[Char,Array[Byte]]
	
	{
		Range(' ','~'+1).foreach(c => CharMap(c.toChar)=Array(c.toByte))
		CharMap('◆') = Array(0x83.toByte)
		CharMap('\n') = Array(0xD.toByte,0xA.toByte)
		CharMap('\r') = Array()
		for((c,i) <- CharTable1.zipWithIndex) if (c!='#') CharMap(c) = Array((i+0x80).toByte)
		for((c,i) <- CharTable2.zipWithIndex) if (c!='#') CharMap(c) = Array((i+0x80).toByte, 0xDE.toByte)
		for((c,i) <- CharTable3.zipWithIndex) if (c!='#') CharMap(c) = Array((i+0x80).toByte, 0xDF.toByte)
		for((c,i) <- CharTable4.zipWithIndex) if (c!='#') CharMap(c) = Array(0x1.toByte, (i+0x40).toByte)
	}
	
	/**
	 * 文字列を、MSX encodingのバイト列に変換します
	 */
	def appendAsMsxText(src:String, buff:Buffer[Byte]) = {
		for((c,i)<-src.zipWithIndex) if (CharMap.contains(c)) buff++=CharMap(c) else throw new IllegalArgumentException("Illegal char: "+c+" near "+src.substring(max(0,i-10),min(i+10,src.length())))
		buff
	}
	
	def convertToMsxText(src:String, appendEOF:Boolean=false) = {
		val result=appendAsMsxText(src, ArrayBuffer())
		(if (appendEOF) result+=0x1A.toByte else result).toArray
	}
	
	/**
	 * プログラムリストの行頭に行番号をつけ、ラベル名を行番号に置換します
	 */
	def convertToLineNumberedCode(lines:Iterable[String], numberToStart:Int=10, numberToStep:Int=10):Array[String] = {
		val labelMap=mutable.Map.empty[String,Int]
		var lineNumber=numberToStart
		val result=new ArrayBuffer[String]
		for(line <- lines) {
			val trimedLine=line.trim()
			if (trimedLine.isEmpty() || (trimedLine.startsWith("'") && !trimedLine.startsWith("''"))) {
				// empty or comment line
			}else if (trimedLine.startsWith("@")) {
				// label line
				labelMap(trimedLine.substring(1))=lineNumber
			}else{
				// normal line
				val line2 = if (trimedLine.startsWith("''")) line.replaceFirst("''","'") else line
				result += lineNumber.toString + " " + line2
				lineNumber += numberToStep
			}
		}
		// replace label references
		val labelR="""@([a-aA-Z0-9_]+)""".r
		result.toArray.map(line => labelR.replaceAllIn(line, m => labelMap.get(m.group(1)).getOrElse(m.group(0)).toString))
	}
	
	def codeToString(lines:Iterable[String]) = lines.mkString("\n")+"\n"
	
	/**
	 * 文字列を改行で分割します
	 */
	def splitToLines(s:String) = s.split("\r?\n")
	
	/**
	 * BASICの文字列リテラルを返します
	 */
	def stringLiteral(s:String) = {
		"\"" + (s.replaceAllLiterally("\"","\"+CHR$(34)+\"").replaceAllLiterally("\n","\"+CHR$(13)+\"")) + "\""
	}
	
	/**
	 * コマンドラインツール
	 */
	def main(args: Array[String]) {
		args(0) match {
			case "convert"=>
				val fout=new java.io.FileOutputStream(args(2))
				fout.write(convertToMsxText(Source.fromFile(args(1)).mkString))
				fout.close()
		}
	}
	
	/**
	 * InputStream to ByteArray
	 */
	def readInputStreamAndClose(is:java.io.InputStream):Array[Byte] = {
		val buff=new Array[Byte](512)
		val baout=new java.io.ByteArrayOutputStream()
		var loop=true;
		while(loop){
			val len=is.read(buff,0,buff.length)
			if (len<0) loop=false else baout.write(buff,0,len)
		}
		is.close()
		baout.toByteArray()
	}
}