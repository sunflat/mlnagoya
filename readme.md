# MSX-BASIC プレゼンツール
* Scalaで書いたスライドのデータを元に、MSX-BASICのコードを生成するツールです
* スライド記述例
	* [MLNagoya.scala](https://bitbucket.org/sunflat/mlnagoya/src/master/src/main/scala/msxtool/example/MLNagoya.scala)
		* [生成されたスライドの実行結果](http://www.slideshare.net/sunflat/msx-language)
	* [SamplePresentation1.scala](https://bitbucket.org/sunflat/mlnagoya/src/master/src/main/scala/msxtool/example/SamplePresentation1.scala)
		* [MSX-BASIC 生成結果(UTF-8版)](https://bitbucket.org/sunflat/mlnagoya/src/master/src/test/resources/msxtool/SamplePresentation1.bas.phase2.txt)
* [補足説明](http://d.hatena.ne.jp/sunflat/20120526/p1)

## 使用方法
* sbt "run -o target/OUTPUT.BAS"
	* msxtool.example パッケージのクラスをmain classとして選ぶ
	* target/OUTPUT.BAS という名前で、MSX-BASICのファイルが出力されます
	* 中間ファイルも同ディレクトリに出力されます

## ライセンス
* 万が一、ソースを再利用したい方がいましたら、MLNagoya.scala 以外は、MITライセンスでご利用ください
* MLNagoya.scala の再利用はご遠慮ください

____
	The MIT License
	
	Copyright (c) 2012 Sunflat
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
